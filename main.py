# importi
import json
import sys
import time
from mpi4py import MPI

# konstante
DEV = True
SD_VALUE = 1.21
ROAD_IS_IN_GOOD_CONDITION = 0
ROAD_IS_IN_BAD_CONDITION = 1

# Strukture
class RoadCondition():
    conditionStatus = -1
    longitude = 0.0
    latitude = 0.0


# globalne spremenljivke
startTime = 0
jsonData = ""

vibrationsData = list()
longitudeData = list()
latitudeData = list()

vibrationsAverage = 0
longitudeAverage = 0
latitudeAverage = 0
sdMaxValue = 0
sdMinValue = 0

comm = MPI.COMM_WORLD
name = MPI.Get_processor_name()
rank = comm.Get_rank()


# fukcije
# povprecje vseh vibracij
def averageOfVibrations(dataList):
    sumVibrations = 0

    for vibration in dataList:
        sumVibrations += vibration

    sumVibrations = sumVibrations / len(dataList)

    if DEV:
        print("Povprecje = ", sumVibrations)

    return sumVibrations


# povprecje lokacije (povprecna lokacija za obmocje meritve)
def averageOfLocation(longitudeList, latitudeList):
    sumLongitude = 0
    sumLatitude = 0

    for lon in longitudeList:
        sumLongitude += lon

    for lat in latitudeList:
        sumLatitude += lat

    #sumLongitude = sumLongitude / len(longitudeList)
    #sumLatitude = sumLatitude / len(latitudeList)

    sumLongitude = longitudeList[-1] 
    sumLatitude = latitudeList[-1]

    if DEV:
        print("Izracun lokacije = ", (sumLongitude, sumLatitude))

    return (sumLongitude, sumLatitude)


# funkcija ki odloci ali je opazovano obmocje dobro ali slabo cestisce
def decideRoadCondition(dataList, sdMaxValue, sdMinValue):
    badRoadCondition = 0
    goodRoadCondition = 0

    # ce je trenutna vrednost blizu povprecja vibracij jo
    # dolocimo kot dobro drugace pa kot slabo
    for i in dataList:
        if (i >= sdMinValue and i <= sdMaxValue):
            goodRoadCondition += 1
        else:
            badRoadCondition += 1
    if DEV:
        print("Slabo = ", badRoadCondition)
        print("Dobro = ", goodRoadCondition)

    # ce je slabih vec kot dobrih odlocimo da je opazovano obmocje
    # slabo cestisce drugace pa odlocimo da je dobro cestisce
    if (badRoadCondition > goodRoadCondition):
        return ROAD_IS_IN_BAD_CONDITION
    else:
        return ROAD_IS_IN_GOOD_CONDITION


# MAIN
# ce kot vhodni parameter dobimo PROD znacko izklopimo DEV nacin
if rank == 0:
    if sys.argv[1] == "PROD":
        DEV = False

    # branje JSON stringa
    if DEV:
        print("Dev")
        startTime = time.time()
        with open('data.json') as json_file:
            jsonData = json.load(json_file)
    else:
        with open('data.json') as json_file:
            jsonData = json.load(json_file)

    # json objekt pretvorimo v tri liste podatkov
    for obj in jsonData:
        vibrationsData.append(obj["z"])
        latitudeData.append(obj["latitude"])
        longitudeData.append(obj["longitude"])

    req01 = comm.isend(vibrationsData, dest=1, tag=100)
    req11 = comm.isend(DEV, dest=1, tag=10)
    req01.wait()
    req11.wait()

if rank == 1:
    req = comm.irecv(source=0, tag=100)
    req1 = comm.irecv(source=0, tag=10)
    vibrationsData = req.wait()
    DEV = req1.wait()
    if DEV:
        print(("name:", name, "my rank is", comm.rank))
    vibrationsAverage = averageOfVibrations(vibrationsData)
    sdMinValue = vibrationsAverage - SD_VALUE
    sdMaxValue = vibrationsAverage + SD_VALUE
    req = comm.isend(vibrationsAverage, dest=0, tag=101)
    req1 = comm.isend(sdMinValue, dest=0, tag=102)
    req2 = comm.isend(sdMaxValue, dest=0, tag=103)
    req.wait()
    req1.wait()
    req2.wait()

if rank == 0:
    req = comm.irecv(source=1, tag=101)
    req1 = comm.irecv(source=1, tag=102)
    req2 = comm.irecv(source=1, tag=103)
    locationAverage = averageOfLocation(longitudeData, latitudeData)
    longitudeAverage = locationAverage[0]
    latitudeAverage = locationAverage[1]
    vibrationsAverage = req.wait()
    sdMinValue = req1.wait()
    sdMaxValue = req2.wait()

    if DEV:
        print(("name:", name, "my rank is", comm.rank))
        print("MAX = ", sdMaxValue)
        print("MIN = ", sdMinValue)

        if (decideRoadCondition(vibrationsData, sdMaxValue, sdMinValue) == ROAD_IS_IN_GOOD_CONDITION):
            print("STANJE = Dobro")
        else:
            print("STANJE = Slabo")

        print("LONGITUDE = ", longitudeAverage)
        print("LATITUDE = ", latitudeAverage)
        print("TIME NEEDED: ", (time.time() - startTime), "seconds")
    else:
        obj = RoadCondition()
        obj.conditionStatus = decideRoadCondition(vibrationsData, sdMaxValue, sdMinValue)
        obj.longitude = longitudeAverage
        obj.latitude = latitudeAverage

        outputString = json.dumps(obj.__dict__)

        print(outputString)
        sys.stdout.flush()